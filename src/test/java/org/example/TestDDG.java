package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestDDG {
    @Test
    public void testDDG() {
        /* DuckDuckGo cause results can be replicated for any user; Firefox one love */
        WebDriver driver = new FirefoxDriver();
        driver.get("https://duckduckgo.com");

        /* Check page title */
        String pageTitle = driver.getTitle();
        System.out.printf("Page Title is %s\n", pageTitle);
        Assert.assertEquals("DuckDuckGo — Privacy, simplified.", pageTitle);

        /* Logo leads to the about page */
        WebElement logo = driver.findElement(By.className("logo_homepage_link"));
        Assert.assertEquals("https://duckduckgo.com/about", logo.getAttribute("href"));

        /* Open menu */
        WebElement menuButton = driver.findElement(By.className("header__button--menu"));
        menuButton.click();
        
        /* Check that menu is visible (is-open class on menu) */
        WebElement menuElement = driver.findElement(By.className("nav-menu--slideout"));
        Assert.assertEquals(true, menuElement.getAttribute("class").contains("is-open"));
        
        /* Fill in the search */
        WebElement searchInput = driver.findElement(By.id("search_form_input_homepage"));
        searchInput.sendKeys("selenium");

        /* Search */
        WebElement searchButton = driver.findElement(By.id("search_button_homepage"));
        searchButton.click();

        /* Check the search */
        searchInput = driver.findElement(By.id("search_form_input"));
        Assert.assertEquals("selenium", searchInput.getAttribute("value"));

        driver.quit();
    }
}